import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import Entity.user;
import java.util.Scanner;

public class createPerson {

  public void createUser(){
    Scanner in = new Scanner(System.in);

    System.out.println("First Name:");
    String firstName = in.nextLine();

    System.out.println("Last Name:");
    String lastName = in.nextLine();

    System.out.println("Result:");
    String result = in.nextLine();

    //create session factory
    SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
            .addAnnotatedClass(user.class)
            .buildSessionFactory();

    //create a session
    Session session = factory.getCurrentSession();

    try{
      //create 3 user objects
      System.out.println("Creating 1 user objects...");
      user tempUser = new user(firstName, lastName, result);

      //start a transaction
      session.beginTransaction();
      System.out.println("Beginning transaction...");

      //save the user object
      session.save(tempUser);
      System.out.println("Saving the new user...");

      //commit the transaction
      session.getTransaction().commit();
      System.out.println("Done!");
    } finally {
      factory.close();
    }
  }
  public void deleteUser(){

    Scanner in = new Scanner(System.in);
    int userID;

    System.out.println("Which user ID number would you like to erase? ");
    userID = in.nextInt();

    //create session factory this is for hibernate
    SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
            .addAnnotatedClass(user.class)
            .buildSessionFactory();

    //create a session this is for hibernate
    Session session = factory.getCurrentSession();

    try{

      //Get a new session and start a transaction
      session = factory.getCurrentSession();
      session.beginTransaction();

      //Retrieve student based on the id: primary key
      System.out.println("\nGetting user with id: " + userID);
      user myUser = session.get(user.class, userID);

      //Delete the student
      //System.out.println("Deleting student: " + myStudent);
      //session.delete(myStudent);

      //Delete student where id=15 this allows you to delete on the fly instead of having to retrieve the object.
      System.out.println("Deleting user where id= " + userID);
      session.createQuery("delete from user where id= " + userID).executeUpdate();

      //commit the transaction
      session.getTransaction().commit();
      System.out.println("Done!");
    } finally {
      factory.close();
    }
  }
}
